package com.CoAP;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.json.JSONException;
import org.json.JSONObject;

import com.iwm.Mote;

public final class CoapGet {

	public Mote getMote(String moteID, String uri) {

		CoapClient client = new CoapClient(uri);
		CoapResponse response = client.get();
		Mote mote = null;

		if (response != null) {	
			Double state, lat, lon;
			try {
				JSONObject jsonObj = new JSONObject(response.getResponseText());
				JSONObject jsonMote = jsonObj.getJSONObject(moteID);
				state=Double.valueOf(jsonMote.getString("Volume"));
				lat=Double.valueOf(jsonMote.getString("Lat"));
				lon=Double.valueOf(jsonMote.getString("Lon"));
				mote = new Mote(moteID, lat, lon, state);
			} catch (JSONException e) {
				System.out.println("Error during Json parsing");
			}
		} else {
			System.out.println("No response received.");
		}

		return mote;
	}
}
