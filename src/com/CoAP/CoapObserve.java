package com.CoAP;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;

import com.iwm.IwmUI;
import com.iwm.NotificationUtils;

public class CoapObserve {
	//private IwmUI ui;
	private final String uri;
	private CoapClient observable;
	private CoapObserveRelation relation;
	
	
	public CoapObserve(final IwmUI ui, String uri){
		//this.ui = ui;
		this.uri = uri;
		observable = new CoapClient(this.uri);
		relation = observable.observe(new CoapHandler() {
			@Override
			public void onLoad(final CoapResponse response) {
				ui.access(new Runnable() {
					@Override
					public void run() {
						NotificationUtils.showNotify(response.getResponseText());
					}
				});
			}
			@Override
			public void onError() {
				ui.access(new Runnable() {
					@Override
					public void run() {
						NotificationUtils.showError("Failed");
					}
				});				
			}
		});
	}
	
	public void cancel(){
		relation.proactiveCancel();
	}
	
	public boolean isCanceled(){
		return relation.isCanceled();
	}
}
