package com.CoAP;

import java.util.Set;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.WebLink;

public class CoapWellKnownCore {
	/* 
	 * 
	 * Usage: 
	 * CoapWellKnownCore.GetCore("coap://vs0.inf.ethz.ch:5683/"); 
	 * 
	*/
	
	public final static void GetCore(String uri){
		CoapClient client = new CoapClient(uri);
		Set<WebLink> links = client.discover();
		for (WebLink link : links) {
			System.out.println(link.getURI());
		}
	}
}
