package com.background;

import java.io.Serializable;
import java.util.Set;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.json.JSONException;
import org.json.JSONObject;

import com.iwm.IwmUI;
import com.iwm.Mote;
import com.iwm.NotificationUtils;

public class MoteDiscover extends Thread implements Serializable {

	private static final long serialVersionUID = 6964594378051642562L;
	private CoapClient client;
	private boolean update;
	// private BeanContainer<String, Mote> motes;
	private final IwmUI ui;
	private final String proxyUri;
	private long timePeriod;

	public MoteDiscover(String uri, IwmUI ui, long timePeriod) {
		super("Mote Discover");

		client = new CoapClient(uri);
		update = true;
		this.ui = ui;
		this.proxyUri = uri;
		this.timePeriod = timePeriod;
		this.start();
		
		System.out.println("New discovering process");
	}

	@Override
	public void run() {
		while (update) {
			Set<WebLink> links = client.discover();
			/* Looking for new motes */
			for (WebLink link : links) {
				final String resUri = link.getURI();
				if (!resUri.equalsIgnoreCase("/.well-known/core") && !resUri.equalsIgnoreCase("/proxy_resource")) {
					final String moteID = link.getURI().replaceFirst("/", "");
					if (!ui.getMotes().containsId(moteID)) {
						System.out.println("New mote: " + moteID + " (" + proxyUri + resUri + ")");
						ui.access(new Runnable() {
							public void run() {
								// Add mote
								Mote mote = getMote(moteID, proxyUri + resUri);
								if (mote != null) {
									ui.getMotes().addBean(mote);
									ui.getGrid().markAsDirtyRecursive();
									new Observer(moteID, proxyUri + resUri, ui);
									ui.getGoogleMap().addMarker(mote.getMarker());
									ui.push();
								} else {
									System.err.println("Error retriving mote information");
								}
							}
						});
					}
				}
			}
			/* Waiting for the next trial */
			try {
				Thread.sleep(timePeriod);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void stopUpdating() {
		update = false;
	}

	private Mote getMote(String moteID, String uri) {
		CoapClient client = new CoapClient(uri);
		CoapResponse response = client.get();
		Mote mote = null;

		if (response != null) {
			Double state, lat, lon;
			try {
				JSONObject jsonObj = new JSONObject(response.getResponseText());
				JSONObject jsonMote = jsonObj.getJSONObject(moteID);
				state = Double.valueOf(jsonMote.getString("Volume"));
				lat = Double.valueOf(jsonMote.getString("Lat"));
				lon = Double.valueOf(jsonMote.getString("Lon"));
				mote = new Mote(moteID, lat, lon, state/100);
			} catch (JSONException e) {
				NotificationUtils.showError("Error during Json parsing");
			}
		} else {
			NotificationUtils.showError("No response received.");
		}

		return mote;
	}

}
