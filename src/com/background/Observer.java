package com.background;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.json.JSONException;
import org.json.JSONObject;

import com.iwm.IwmUI;
import com.iwm.Mote;
import com.iwm.NotificationUtils;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.Grid;

public class Observer {

	private CoapClient observable;
	private CoapObserveRelation relation;
	private final String uri;
	private final String moteID;
	private BeanContainer<String, Mote> motes;
	private GoogleMap gmap;
	private Grid grid;

	//private Random rnd;

	public Observer(final String moteID, final String uri, final IwmUI ui) {
		super();
		System.out.println("New observation to: " + moteID);
		//rnd = new Random();
		this.uri = uri;
		this.moteID = moteID;
		this.motes = ui.getMotes();
		this.gmap = ui.getGoogleMap();
		this.grid = ui.getGrid();
		observable = new CoapClient(this.uri);
		relation = observable.observe(new CoapHandler() {
			@Override
			public void onLoad(final CoapResponse response) {
				ui.access(new Runnable() {
					@SuppressWarnings("unchecked")
					@Override
					public void run() {
						/* Mote state update */
						// BeanItem<Mote> bean =
						// motes.getItem(motes.getIdByIndex(moteIndex));
						BeanItem<Mote> bean = motes.getItem((Object) moteID);
						// New state retrivial
						bean.getItemProperty("state").setValue(getMoteState(response.getResponseText()));
						grid.markAsDirtyRecursive();
						/* Google Map update */
						Mote mote = (Mote) bean.getBean();
						GoogleMapMarker marker = mote.getMarker();
						if (!marker.getIconUrl().toString().equalsIgnoreCase(mote.getTagUrl())) {
							gmap.removeMarker(marker);
							gmap.addMarker(mote.regenerateMarker());
						}
						// Push updates to client UI
						ui.push();
					}
				});
			}

			@Override
			public void onError() {
				ui.access(new Runnable() {
					@Override
					public void run() {
						NotificationUtils.showError("An error occured during CoAP observing to " + uri);
					}
				});
			}
		});
	}

	public void cancel() {
		relation.proactiveCancel();
	}

	public boolean isCanceled() {
		return relation.isCanceled();
	}
	
	private Double getMoteState(String jsonData){
		Double state = 0.0;
		if (jsonData != null) {
			try {
				JSONObject jsonMote = new JSONObject(jsonData).getJSONObject(moteID);
				state = Double.valueOf(jsonMote.getString("Volume"))/100;
			} catch (JSONException e) {
				NotificationUtils.showError("Error during Json parsing");
			}
		}
		return state;
	}

}
