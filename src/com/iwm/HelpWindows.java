package com.iwm;


import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class HelpWindows extends Window {
	private static final long serialVersionUID = 1193921443281608002L;
	private final VerticalLayout layout = new VerticalLayout();

	@SuppressWarnings("deprecation") //due to raw html content
	public HelpWindows() {
		super("Help");
		this.center();
		layout.setSpacing(true);
		layout.setStyleName("helpWin");
		/* General overview */
		Label brief = new Label("<p>General overview</p>" + "<ul>"
				+ "<li>Click on geo-tag for showing the mote name</li>"
				+ "<li>Double click on the mote name in the table to show detail about that mote</li>"
				+ "<li>Select at least one mote and generate direction by clicking to \"Get Direction\"</li>" + "</ul>",
				Label.CONTENT_RAW);
		brief.setWidth("80%");
		/* Color legend */
		Label legendTitle = new Label("<p>Color Legend</p>", Label.CONTENT_RAW);
		Image legend = new Image(null, new ThemeResource("imgs/legend.png"));
		/* Add to layout */
		layout.addComponent(brief);
		layout.addComponent(legendTitle);
		layout.addComponent(legend);
		layout.setComponentAlignment(legend, Alignment.BOTTOM_CENTER);
		this.setContent(layout);
	}
}
