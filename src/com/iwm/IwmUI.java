package com.iwm;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.annotation.WebServlet;

import com.background.MoteDiscover;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.GoogleMapControl;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.events.MarkerClickListener;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.renderers.ProgressBarRenderer;

@SuppressWarnings("serial")
@Theme("iwm")
@Push(PushMode.MANUAL)
public class IwmUI extends UI {
	/* General Parameters */
	public final Double MAXLOAD = 3500.0; // Liters

	private final DecimalFormat df = new DecimalFormat("#.##");
	private Grid motesGrid;
	private BeanContainer<String, Mote> motes;
	private GoogleMap gmap; /*
							 * ref:
							 * https://vaadin.com/directory#!addon/googlemaps-
							 * add-on
							 */

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = IwmUI.class, widgetset = "com.iwm.widgetset.IwmWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		/* ---- Mote list init---- */
		motes = new BeanContainer<String, Mote>(Mote.class);
		motes.setBeanIdProperty("name");
		//buildExampleList();

		/* ---- Layouts ---- */
		AbsoluteLayout layout = new AbsoluteLayout();
		layout.setWidth("99%");
		layout.setHeight("99%");
		setContent(layout);
		AbsoluteLayout sidebar = new AbsoluteLayout();
		sidebar.setWidth("300px");
		sidebar.setHeight("100%");
		layout.addComponent(sidebar, "left: 20px; top: 20px;");

		/* ---- UI init ---- */
		initMotesGrid(sidebar);
		initHelpButton(sidebar);
		initGetDirectionButton(sidebar);
		initGoogleMaps(layout, 43.7182778, 10.4018889);

		/* Populate Grid & Map */
		motesGrid.setContainerDataSource(motes);
		gmap.clearMarkers();
		/*for (Object itemId : motes.getItemIds()) {
			Mote m = (Mote) motes.getItem(itemId).getBean();
			gmap.addMarker(m.getMarker());
		}*/

		/* Californium */
		/*ant = new Observer("M1", "coap://vs0.inf.ethz.ch:5683/obs", (IwmUI) UI.getCurrent());
		ant = new Observer("M2", "coap://vs0.inf.ethz.ch:5683/obs", (IwmUI) UI.getCurrent());
		ant = new Observer("M3", "coap://vs0.inf.ethz.ch:5683/obs", (IwmUI) UI.getCurrent());
		ant = new Observer("M4", "coap://vs0.inf.ethz.ch:5683/obs", (IwmUI) UI.getCurrent());
		ant = new Observer("M5", "coap://vs0.inf.ethz.ch:5683/obs", (IwmUI) UI.getCurrent());
		ant = new Observer("M6", "coap://vs0.inf.ethz.ch:5683/obs", (IwmUI) UI.getCurrent());*/
		new MoteDiscover("coap://[aaaa::c30c:0:0:2]", (IwmUI) UI.getCurrent(), 3000);
		new MoteDiscover("coap://[aaaa::c30c:0:0:3]", (IwmUI) UI.getCurrent(), 3000);
	}

	private void initMotesGrid(AbsoluteLayout layout) {
		motesGrid = new Grid();
		motesGrid.setImmediate(true);
		motesGrid.setResponsive(true);
		motesGrid.setWidth("300px");
		motesGrid.setHeight("92%");
		motesGrid.setSelectionMode(SelectionMode.MULTI);
		motesGrid.addColumn("name", String.class);
		motesGrid.addColumn("state", Double.class).setRenderer(new ProgressBarRenderer()).setExpandRatio(2);
		layout.addComponent(motesGrid, "top: 0; left:0;");

		motesGrid.addItemClickListener(new ItemClickListener() {
			@Override
			public void itemClick(ItemClickEvent event) {
				// if (event.isDoubleClick()) {
				Window moteWin = new MoteWindow((Mote) motes.getItem(event.getItemId()).getBean());
				UI.getCurrent().addWindow(moteWin);
				// }
			}
		});
	}

	private void initGetDirectionButton(AbsoluteLayout layout) {
		Button directions = new Button("Get Directions", FontAwesome.CAR);
		directions.setHeight("40px");
		directions.setWidth("220px");
		directions.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				double load = 0.0;
				Mote m;
				Collection<Object> sel = motesGrid.getSelectedRows();
				if (sel.size() > 0) {
					Waypoints wp = new Waypoints();
					Iterator<Object> itr = sel.iterator();
					while (itr.hasNext()) {
						m = (Mote) motes.getItem(itr.next()).getBean();
						load += m.getState() * m.CAPACITY;
						wp.addWaypoint(m);
					}
					if (load < MAXLOAD) {
						Window window = new Window();
						window.setResizable(true);
						window.setDraggable(true);
						window.setSizeFull();
						window.center();
						BrowserFrame browser = new BrowserFrame("Directions",
								new ExternalResource(wp.getQueryUrl(gmap.getCenter(), gmap.getZoom())));
						browser.setSizeFull();
						window.setContent(browser);
						UI.getCurrent().addWindow(window);
					} else
						NotificationUtils.showError("The selected volume exceed the mamimum allowed");
				} else
					NotificationUtils.showError("Please select at least one node...");
			}
		});
		layout.addComponent(directions, "bottom:5px; left:80px;");
	}

	private void initHelpButton(AbsoluteLayout layout) {
		Button help = new Button(FontAwesome.QUESTION);
		help.setHeight("40px");
		help.setWidth("75px");
		help.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Window helpWin = new HelpWindows();
				UI.getCurrent().addWindow(helpWin);
			}
		});
		layout.addComponent(help, "bottom:5px; left:0;");
	}

	public void initGoogleMaps(AbsoluteLayout layout, double lat, double lon) {
		gmap = new GoogleMap(null, null, "italian");
		gmap.setImmediate(true);
		gmap.setSizeFull();
		gmap.setHeight("99%");
		gmap.setMinZoom(10);
		gmap.setMaxZoom(20);
		gmap.setZoom(14);
		gmap.setCenter(new LatLon(lat, lon));
		gmap.removeControl(GoogleMapControl.MapType);
		gmap.addMarkerClickListener(new MarkerClickListener() {
			@Override
			public void markerClicked(GoogleMapMarker clickedMarker) {
				if (clickedMarker != null) {
					Mote mote = (Mote) motes.getItem(clickedMarker.getCaption()).getBean();
					NotificationUtils.showNotify(mote.getName() + ": " + df.format(mote.getState()) + "%");
				}
			}
		});
		layout.addComponent(gmap, "left: 340px; top:20px;");
	}

	public Grid getGrid() {
		return this.motesGrid;
	}

	public GoogleMap getGoogleMap() {
		return this.gmap;
	}

	public BeanContainer<String, Mote> getMotes() {
		return this.motes;
	}

	/*private void buildExampleList() {
		motes.addBean(new Mote("M1", 43.7131111, 10.4002500, 0.0));
		motes.addBean(new Mote("M2", 43.7182778, 10.4018889, 0.0));
		motes.addBean(new Mote("M3", 43.7197778, 10.4073333, 0.0));
		motes.addBean(new Mote("M4", 43.7233056, 10.4021111, 0.0));
		motes.addBean(new Mote("M5", 43.716363, 10.3946389, 0.0));
		motes.addBean(new Mote("M6", 43.720270, 10.384220, 0.0));
		motes.addBean(new Mote("M7", 43.723475, 10.411822, 0));
		motes.addBean(new Mote("M8", 43.714668, 10.414731, 0.0));
		motes.addBean(new Mote("M9", 43.712570, 10.395265, 0.0));
		motes.addBean(new Mote("M10", 43.710476, 10.403011, 0.0));
	}*/
}