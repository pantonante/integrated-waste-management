package com.iwm;

import java.io.Serializable;

import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;

public class Mote implements Serializable{
	
	/*
	 * Mote Bean
	 */
	private static final long serialVersionUID = 1L;
	public static final String tag_base = "VAADIN/tags/";
	public static final String BlueTag = tag_base+"blue.png";
	public static final String GreenTag = tag_base+"green.png";
	public static final String OrangeTag = tag_base+"orange.png";
	public static final String RedTag = tag_base+"red.png";
	
	private String name;
	private LatLon latlon;
	private double state; //[0,1]
	private GoogleMapMarker marker;
	
	public final Double CAPACITY = 1000.0; //Liters
	
	public Mote(String name, double lat, double lon, double state){
		this.name=name;
		this.latlon = new LatLon(lat, lon);
		this.state = state;
		this.marker = new GoogleMapMarker(name, this.latlon, false, getTagUrl());
		this.marker.setAnimationEnabled(false);
	}
	
	public Mote(String name, LatLon ll, double state){
		this.name=name;
		this.latlon=ll;
		this.state = state;
		this.marker = new GoogleMapMarker(name, this.latlon, false, getTagUrl());
		this.marker.setAnimationEnabled(false);
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setLatLon(double lat, double lon){
		this.latlon = new LatLon(lat, lon);
	}
	
	public void setState(double state){
		this.state=state;
	}
	
	public String getName(){
		return this.name;
	}
	
	public LatLon getLatLon(){
		return this.latlon;
	}
	
	public double getState(){
		return this.state;
	}
	
	public GoogleMapMarker getMarker(){
		return this.marker;
	}
	
	public GoogleMapMarker regenerateMarker(){
		this.marker = new GoogleMapMarker(this.name, this.latlon, false, getTagUrl());
		this.marker.setAnimationEnabled(false);
		return this.marker;
	}
	
	public String getTagUrl(){
		String tagUrl=null;
		if(this.state<0.25)
			tagUrl=BlueTag;
		else if(this.state>=0.25 && this.state<0.5)
			tagUrl=GreenTag;
		else if(this.state>=0.5 && this.state<0.75)
			tagUrl=OrangeTag;
		else
			tagUrl=RedTag;
		return tagUrl;
	}
}
