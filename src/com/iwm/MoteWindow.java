package com.iwm;

import java.text.DecimalFormat;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class MoteWindow extends Window {
	private static final long serialVersionUID = 1L;
	private FormLayout content;
	private TextField name;
	private TextField lat;
	private TextField lon;
	private TextField state;

	private final DecimalFormat df = new DecimalFormat("#.##");

	public MoteWindow(Mote mote) {
		super(mote.getName());
		this.center();
		this.setResizable(false);

		this.content = new FormLayout();
		this.content.setSizeUndefined();
		this.content.setMargin(true);
		this.content.setSpacing(true);

		this.name = new TextField("Name");
		this.name.setValue(mote.getName());
		this.name.setImmediate(false);
		this.name.setReadOnly(true);
		this.content.addComponent(this.name);

		this.lat = new TextField("Latitude");
		this.lat.setValue(String.valueOf(mote.getLatLon().getLat()));
		this.lat.setImmediate(false);
		this.lat.setReadOnly(true);
		this.content.addComponent(this.lat);

		this.lon = new TextField("Longitute");
		this.lon.setValue(String.valueOf(mote.getLatLon().getLon()));
		this.lon.setImmediate(false);
		this.lon.setReadOnly(true);
		this.content.addComponent(this.lon);

		this.state = new TextField("State");
		this.state.setValue(df.format(mote.getState() * 100) + " %");
		this.state.setImmediate(false);
		this.state.setReadOnly(true);
		this.content.addComponent(this.state);

		this.setContent(this.content);
	}
}
