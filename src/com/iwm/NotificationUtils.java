package com.iwm;

import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class NotificationUtils {
	
	public static void showError(String message) {
		Notification notif = new Notification("Errore", message,
				Notification.Type.ERROR_MESSAGE);
		notif.setDelayMsec(5000);
		notif.setPosition(Position.TOP_CENTER);
		notif.show(Page.getCurrent());
	}

	public static void showNotify(String message) {
		Notification notif = new Notification("Notifica", message,
				Notification.Type.TRAY_NOTIFICATION);
		notif.setDelayMsec(5000);
		notif.setPosition(Position.TOP_RIGHT);
		notif.show(Page.getCurrent());
	}

}
