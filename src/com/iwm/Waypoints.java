package com.iwm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.vaadin.tapio.googlemaps.client.LatLon;

public final class Waypoints {
	private static final String base_url = "https://maps.here.com/directions/mix";
	private List<Mote> waypointsList;
	private LatLon start = null;
	private LatLon stop = null;
	
	public Waypoints() {
		waypointsList = new ArrayList<Mote>();
	}
	
	public Waypoints(LatLon start, LatLon stop){
		waypointsList = new ArrayList<Mote>();
		this.start = start;
		this.stop = stop;
	}

	public void addWaypoint(Mote m) {
		waypointsList.add(m);
	}

	public void clear() {
		waypointsList.clear();
	}

	public String getQueryUrl(LatLon center, int zoom) {
		/* Sorting */
		Collections.sort(waypointsList, new Comparator<Mote>() {
			@Override
			public int compare(Mote m1, Mote m2) {
				if (m1.getState() < m2.getState())
					return 1;
				else if (m1.getState() > m2.getState())
					return -1;
				else
					return 0;
			}
		});
		/* URL generation */
		String url = base_url;
		if(start!=null){
			url += "/" + String.valueOf(start.getLat()) + ",";
			url += String.valueOf(start.getLon()) + ":START";
		}
		for (int i = 0; i < waypointsList.size(); i++) {
			Mote m = waypointsList.get(i);
			url += "/" + String.valueOf(m.getLatLon().getLat()) + ",";
			url += String.valueOf(m.getLatLon().getLon()) + ":" + m.getName();
		}
		if(stop!=null){
			url += "/" + String.valueOf(stop.getLat()) + ",";
			url += String.valueOf(stop.getLon()) + ":STOP";
		}
		url += "?map=" + center.getLat() + "," + center.getLon() + "," + zoom + ",normal";
		return url;
	}
}
